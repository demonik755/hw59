import React, {Component} from  'react';
import axios from 'axios';
import './App.css';
class App extends Component {
  state = {
    jokes: "Humans believe in GOD.GOd believes in DEATH.DEATH believes in CHUCK NORRIS "
  };
  getJoke = ()  => {
      console.log();
    axios.get('https://api.chucknorris.io/jokes/random')
        .then(response => {
            console.log(response);
          this.setState({jokes: response.data.value});
        }).catch(error => {
      console.log(error);
    });
  };
  render() {
  return (
    <div className="App">
      <h1>Chuck Norris jokes</h1>
      <h3>{this.state.jokes}</h3>
      <button className="btn" type="button" onClick={()=>this.getJoke()}>More Chuck Jokes!</button>

    </div>
  );
  }
}
export default App;
